package goerr

import "github.com/pkg/errors"

type Unauthorized struct {
	BaseError
}

func NewUnauthorized(e error) Unauthorized {
	return Unauthorized{NewBaseError(e)}
}

func (e Unauthorized) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e Unauthorized) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewUnauthorized(newErr)
}
