package goerr

import "github.com/pkg/errors"

type NotFound struct {
	BaseError
}

func NewNotFound(e error) NotFound {
	return NotFound{NewBaseError(e)}
}

func (e NotFound) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e NotFound) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewNotFound(newErr)
}

func (e NotFound) Cause() error {
	return e.err
}
