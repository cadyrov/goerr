package goerr

import "github.com/pkg/errors"

type Conflict struct {
	BaseError
}

func NewConflict(e error) Conflict {
	return Conflict{NewBaseError(e)}
}

func (e Conflict) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e Conflict) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewConflict(newErr)
}

func (e Conflict) Cause() error {
	return e.err
}
