package goerr

import "github.com/pkg/errors"

type NotAcceptable struct {
	BaseError
}

func NewNotAcceptable(e error) NotAcceptable {
	return NotAcceptable{NewBaseError(e)}
}

func (e NotAcceptable) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e NotAcceptable) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewNotAcceptable(newErr)
}

func (e NotAcceptable) Cause() error {
	return e.err
}
