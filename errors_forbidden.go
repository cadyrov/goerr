package goerr

import "github.com/pkg/errors"

type Forbidden struct {
	BaseError
}

func NewForbidden(e error) Forbidden {
	return Forbidden{NewBaseError(e)}
}

func (e Forbidden) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e Forbidden) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewForbidden(newErr)
}

func (e Forbidden) Cause() error {
	return e.err
}
