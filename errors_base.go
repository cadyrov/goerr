package goerr

type BaseError struct {
	err error
}

func NewBaseError(e error) BaseError {
	return BaseError{err: e}
}

func (e BaseError) Error() string {
	if e.err == nil {
		return ""
	}

	return e.err.Error()
}

func (e BaseError) Unwrap() error { return e.err }

func (e BaseError) Cause() error {
	return e.err
}
