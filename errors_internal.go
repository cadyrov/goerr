package goerr

import "github.com/pkg/errors"

type Internal struct {
	BaseError
}

func NewInternal(e error) Internal {
	return Internal{NewBaseError(e)}
}

func (e Internal) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e Internal) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewInternal(newErr)
}

func (e Internal) Cause() error {
	return e.err
}
