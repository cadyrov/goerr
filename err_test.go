package goerr

import (
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestErr(t *testing.T) {
	t.Parallel()

	var (
		TestError     = errors.New("test")
		TestDenyError = errors.New("deny")
	)

	ex := BadRequest{}.Wrap(TestError, "test wrap 1")

	exErr := errors.Wrap(ex, "test wrap 2")

	assert.True(t, errors.As(exErr, &BadRequest{}))
	assert.True(t, errors.Is(exErr, TestError))
	assert.False(t, errors.Is(exErr, TestDenyError))
	assert.Equal(t, "test", errors.Cause(exErr).Error())

	un := Unauthorized{}.Wrap(TestError, "test wrap 1")

	unErr := errors.Wrap(un, "test wrap 2")

	assert.True(t, errors.As(unErr, &Unauthorized{}))
	assert.True(t, errors.Is(unErr, TestError))
	assert.False(t, errors.Is(unErr, TestDenyError))
	assert.Equal(t, "test", errors.Cause(unErr).Error())

	assert.False(t, errors.As(unErr, &BadRequest{}))
}
