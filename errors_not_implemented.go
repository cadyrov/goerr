package goerr

import "github.com/pkg/errors"

type NotImplemented struct {
	BaseError
}

func NewNotImplemented(e error) NotImplemented {
	return NotImplemented{NewBaseError(e)}
}

func (e NotImplemented) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e NotImplemented) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewNotImplemented(newErr)
}
