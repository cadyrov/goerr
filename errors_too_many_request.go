package goerr

import "github.com/pkg/errors"

type TooManyRequest struct {
	BaseError
}

func NewTooManyRequest(e error) TooManyRequest {
	return TooManyRequest{NewBaseError(e)}
}

func (e TooManyRequest) Wrap(err error, msg string) error {
	return e.Wrapf(err, msg)
}

func (e TooManyRequest) Wrapf(err error, msg string, args ...interface{}) error {
	if err == nil {
		return nil
	}

	newErr := errors.Wrapf(err, msg, args...)

	return NewTooManyRequest(newErr)
}

func (e TooManyRequest) Cause() error {
	return e.err
}
